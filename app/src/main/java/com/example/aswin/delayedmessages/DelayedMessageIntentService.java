package com.example.aswin.delayedmessages;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.MainThread;

public class DelayedMessageIntentService extends IntentService {

    Handler mainHandler;
    public DelayedMessageIntentService() {
        super("DelayedMessageIntentService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            final String message = intent.getStringExtra("message");
            Thread.sleep(5000);

            // 1. not working. Why?
//            Toast.makeText(DelayedMessageIntentService.this, message, Toast.LENGTH_LONG).show();

            // 2. working
            mainHandler = new Handler(DelayedMessageIntentService.this.getMainLooper());
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(DelayedMessageIntentService.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
